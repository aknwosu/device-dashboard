import React, { Component } from 'react'
import ReadingDetail from './ReadingDetail'
import axios from 'axios'

class DeviceReadingList extends Component {
  constructor(props) {
    super(props)
    this.findReadingName = this.findReadingName.bind(this)
    this.updateDevices = this.updateDevices.bind(this);
    this.toggleDeviceState = this.toggleDeviceState.bind(this);
    this.sortDevices=this.sortDevices.bind(this)
    this.state = { 
      deviceList: [], 
      searchTerm: "",
      messageText: "",
      sortBy: "name",
      sortDirection: "asc"
    }
  }

  componentDidMount() {
    axios.get(`http://127.0.0.1:8888/device`)
      .then((response) => {
        this.setState({
          deviceList: response.data.data
        })
      }).catch((err) => {
        console.log(err)
      })
  }

  renderDevice() {
    const renderedDevices =
    this.state.deviceList.filter(device => device.name.includes(this.state.searchTerm.toLowerCase())) 
    return renderedDevices.map((device, i) => 
      (<ReadingDetail 
          key={i}
          device={device}
          toggleState={this.toggleDeviceState}
        />)
    )
  }

  findReadingName(e){
    this.setState({searchTerm: e.target.value})
  }

  toggleDeviceState(deviceName, currentState) {
    const updatedState = currentState ? "false": "true"
    axios.patch(`http://127.0.0.1:8888/device/${deviceName}?active=${updatedState}`)
    .then((response) => {
      if (response.status === 200) {
        this.setState({messageText: "Updated successfully"})
        this.updateDevices();
      } 
    }).catch((err) => {
      this.setState({messageText: "An error occured, please try again"})
    })
  }

  updateDevices() {
    axios.get(`http://127.0.0.1:8888/device`)
      .then((response) => {
        this.setState({deviceList: response.data.data})
      }).catch((err) => {
        console.log(err);
      })
  }

  sortDevices(sortValue) {
    let dir = -1
    if (sortValue === this.state.sortBy) {
      dir = this.state.sortDirection === "asc" ? -1 : 1
    }
    const sortedDeviceReading = this.state.deviceList.sort(function (obj1, obj2) {
      if (obj1[sortValue] < obj2[sortValue]) {
        return -1 * dir;
      }
      if (obj1[sortValue] > obj2[sortValue]) {
        return 1 * dir;
      }
      return 0;
    })
    this.setState({
      deviceList: sortedDeviceReading,
      sortBy: sortValue,
      sortDirection: dir === 1 ? 'asc' : 'desc'
    })
  }
  render() {
    const activeDevices = this.state.deviceList.filter((device) => device.active ).length
    const renderedDevices = this.state.deviceList.filter(device => device.name.includes(this.state.searchTerm)) 
    return (
      <div>
        <div className="deviceState">
          <h4 className="activeCount">Active Device Readings: {activeDevices}</h4>
          <h4 className="inactiveCount">Inactive Device Readings: {this.state.deviceList.length - activeDevices}</h4>
        </div>
        <label htmlFor="readingName">Search by reading name:  </label>
        <input name="readingName" type="text" onChange={this.findReadingName} />
        <div>{this.state.messageText}</div>
        <table>
          <thead>
            <tr>
              <th onClick={()=>this.sortDevices("name")}>Reading Name</th>
              <th onClick={()=>this.sortDevices("unit")}>Unit</th>
              <th onClick={()=>this.sortDevices("value")}>Value</th>
              <th onClick={()=>this.sortDevices("timestamp")}>TimeStamp</th>
              <th onClick={()=>this.sortDevices("active")}>Status</th>
              <th>Toggle State</th>
            </tr>
          </thead>
          <tbody>
            {this.state.deviceList.length ? this.renderDevice() : <tr><td>Loading...</td></tr>}
          </tbody>
        </table>
      </div>
    )
  }
}
export default DeviceReadingList