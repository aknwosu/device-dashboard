
import React from 'react'

export const ReadingDetail = (props) => {
  const { active, name, timestamp, unit, value } = props.device
  const status =  active ? "active": "inactive"
  return(
    <tr className={`device${status}`}>
      <td>{name}</td>
      <td>{unit}</td>
      <td>{value}</td>
      <td>{timestamp}</td>
      <td>{status}</td>
      <td><button onClick={() => props.toggleState(name, active)}>Toggle Status</button></td>
    </tr>

  )
}
export default ReadingDetail