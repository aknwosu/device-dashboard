import React from 'react';
import DeviceReadingList from './Device/DeviceReadingList'

function App() {
    return (
        <div>
            <h1>Relayr Device Dashboard</h1>
            <DeviceReadingList />
        </div>
    );
}

export default App;
